(** Check program mode(master/worker) based on command line argument *)

open Channel.Socket

type 'a t =
  | Master of address * int
  | Worker of address

exception InvalidMode

let _get op =
  match op with
  | Some x -> x
  | None -> raise InvalidMode
;;

let check_host host = _get @@ check_host host
let check_port port = _get @@ int_of_string_opt port

(** Check execution mode by inspecting program arguments.contents

    @return Mastr/Worker mode with corresponding socket address.
    @raise InvalidMode if argument is not correct *)
let check () =
  let argv = Sys.argv in
  let argc = Array.length argv - 1 in
  match argc with
  | 3 ->
    let host, port = check_host argv.(2), check_port argv.(3) in
    let address = address_of host port in
    (match argv.(1) with
     | "master" -> Master (address, 3)
     | "worker" -> Worker address
     | _ -> raise InvalidMode)
  | _ -> raise InvalidMode
;;
