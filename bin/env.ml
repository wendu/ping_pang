open Log
module IntMap = Hashtbl.Make (Int)
open Channel
open Eio

type address_map = Socket.address IntMap.t

type env =
  { mutable id : int
  ; mutable count : int
  ; mutable locations : address_map
  }

type actor_env =
  { id : int
  ; count : int
  }

type t =
  { actor : actor_env
  ; eio : Eio_unix.Stdenv.base
  }

let private_env : env = { id = 0; count = 0; locations = IntMap.create 0 }
let me () = private_env.id
let self_address () = IntMap.find private_env.locations (me ())

let print_env (env : env) =
  trace "Node ID: %d" env.id;
  trace "Node count: %d" env.count;
  trace "Node locations:";
  IntMap.iter
    (fun id adr -> trace "[%d] -> [%s]" id (Socket.string_of_address adr))
    env.locations;
  trace "===============================";
  trace "Environment setup success !";
  trace "==============================="
;;

module Setup = struct
  type 'a master_message =
    | Id of int
    | Ready of int * address_map

  type client_message = Hello

  (** Register a client in master node, then send id to client

      @param channel for communication with the client *)
  let register (counter : Client_counter.t) flow (adress : Net.Sockaddr.stream) =
    trace "new client connection";
    let message = Channel.read flow in
    trace "message read";
    match message with
    | Hello ->
      Client_counter.increase counter;
      let client_id = counter.n in
      traceln "receive hello from [%d]" client_id;
      IntMap.add private_env.locations client_id adress;
      Channel.write flow (Id client_id);
      (* wait until all clients are connected *)
      Client_counter.wait_all counter;
      (* send message to go *)
      Channel.write flow (Ready (private_env.count, private_env.locations));
      Log.trace "Master close connection"
  ;;

  (** Master waiting [count - 1] TCP clients at [address].

      After this function, the environment has [count] nodes,
      including master. *)
  let master net (address : Socket.address) count =
    private_env.count <- count;
    IntMap.add private_env.locations 0 address;
    Log.trace "Master setting up";
    Channel.poll_until net address (count - 1) register;
    Log.trace "master setup stop polling"
  ;;

  (** Worker setup, recv id and signal to launch *)
  let worker net address =
    Switch.run
    @@ fun sw ->
    Switch.on_release sw (fun () -> Log.trace "worker setup switch released");
    let flow = Eio.Net.connect ~sw net address in
    traceln "connected from to master";
    Channel.write flow Hello;
    traceln "hello written";
    (* for ID *)
    let res = Channel.read flow in
    (match res with
     | Id n -> private_env.id <- n
     | _ -> failwith "not expecting message");
    let msg = Channel.read flow in
    (* for ready and every nodes' address *)
    (match msg with
     | Ready (n, map) ->
       private_env.count <- n;
       private_env.locations <- map
     | _ -> failwith "not expecting message");
    Log.trace "worker close connection"
  ;;
end

let run (main : t -> unit) : unit =
  Eio_main.run
  @@ fun env ->
  let net = Eio.Stdenv.net env in
  (try
     match Mode.check () with
     | Master (address, count) ->
       trace "Running in [Master] mode";
       Setup.master net address count
     | Worker address ->
       trace "Running in [Worker] mode";
       Setup.worker net address
   with
   | Mode.InvalidMode -> trace "invalid mode");
  print_env private_env;
  let actor_env = { id = private_env.id; count = private_env.count } in
  let eio_env = env in
  let app_env = { actor = actor_env; eio = eio_env } in
  main app_env
;;
