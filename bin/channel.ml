open Eio

module Socket = struct
  (** Specilization of Unix socket to TCP IP v4 socket *)

  open Unix

  type t = file_descr
  type 'a host = 'a Eio.Net.Ipaddr.t
  type port = int
  type address = Eio.Net.Sockaddr.stream

  (** Sockets on Internet by IP_v4 *)
  let _domain = PF_INET

  (** Reliable byte stream *)
  let _channel_type = SOCK_STREAM

  (** 0 means using default protocol based on domain and channel.

      In our case TCP. *)
  let _protocol = 0

  (** Create an empty TCP socket, with no adress attached *)
  let _create () =
    let s = Unix.socket _domain _channel_type _protocol in
    Unix.setsockopt s Unix.SO_REUSEPORT true;
    s
  ;;

  (** Validates a string host *)
  let check_host str : 'a host option =
    try
      let host = Unix.gethostbyname str in
      let ip = host.h_addr_list.(0) in
      Some (Eio_unix.Net.Ipaddr.of_unix ip)
    with
    | Not_found -> None
  ;;

  (** host + port = address *)
  let address_of (host : 'a host) (port : port) : address = `Tcp (host, port)

  let string_of_address adr =
    match adr with
    | `Tcp (host, port) -> Format.asprintf "%a:%d" Eio.Net.Ipaddr.pp host port
    | _ -> failwith "not expected address type"
  ;;

  let connect_to net (address : address) f =
    (* basic solution dealing with TCP retry *)
    let n = ref 0 in
    let rec infinit_connect sw =
      try
        n := !n + 1;
        if !n = 5 then failwith "tcp failed after 5 retry" else ();
        let flow = Eio.Net.connect ~sw net address in
        flow
      with
      | _ -> infinit_connect sw
    in
    Switch.run
    @@ fun sw ->
    let flow = infinit_connect sw in
    f flow
  ;;
end

(** Read and write on socket *)

(** A channel has 2 attributes, input channel, output channel for read and write.
    The channel is not buffered, other side will receive data immediately *)

let read flow =
  let buffer = Buf_read.of_flow flow ~max_size:1_000_000 in
  let size = Buf_read.uint8 buffer in
  traceln "Read: [%d] bytes data" size;
  let n_buffer_parser = Buf_read.take size in
  let object_bytes = n_buffer_parser buffer in
  Marshal.from_string object_bytes 0
;;

let write flow value =
  let data_in_bytes = Marshal.to_bytes value [] in
  let size = Bytes.length data_in_bytes in
  traceln "Write: data size [%d] bytes" size;
  Buf_write.with_flow flow (fun w ->
    Buf_write.uint8 w size;
    Buf_write.bytes w data_in_bytes)
;;

module Client_counter = struct
  (* Note: not safe to share across domains! *)

  type t =
    { mutable n : int
    ; max : int
    ; changed : Condition.t
    }

  let make max = { n = 0; max; changed = Condition.create () }

  let wait_all t =
    while t.n <> t.max do
      Condition.await_no_mutex t.changed
    done
  ;;

  let increase t =
    if t.n >= t.max then failwith "more clients than expected" else t.n <- t.n + 1;
    Condition.broadcast t.changed
  ;;
end

(** Create a server polling at [adr] until [n] clients has connected.
    When n client has connected, any new connection will be shutdown immediately.
    The server will terminate when all flows are closed. *)
let poll_until net adr n handler =
  let promise, resolver = Promise.create () in
  let counter = Client_counter.make n in
  Fiber.both
    (* shutdown server when condition is met *)
      (fun () ->
      Client_counter.wait_all counter;
      Promise.resolve resolver true)
    (* listen for connections *)
      (fun () ->
      Switch.run
      @@ fun sw ->
      Switch.on_release sw (fun () -> Log.trace "Polling switch realeased");
      let socket = Eio.Net.listen ~reuse_port:true ~sw ~backlog:5 net adr in
      let _ =
        Eio.Net.run_server
          ~stop:promise
          socket
          (handler counter)
          ~on_error:(traceln "Error on polling: %a" Fmt.exn)
      in
      traceln "Server finish")
;;
