open Eio

let main (env : Env.t) =
  print_endline "hello world";
  let net = Eio.Stdenv.net env.eio in
  let res = Node.call net 1 0 "plus_1" 1 in
  traceln "node %d get %d" env.actor.id res;
  traceln "node %d get %d" env.actor.id res
;;

(* let res = Node.call 1 0 plus_1 100 in
   print_int res;
   print_endline "";
   let x = Node.define 0 10 in
   Node.print_value x;
   print_endline "" *)

let _ = Env.run main
