(* open Channel *)

open Channel

type _identity =
  | Me
  | NotMe

let _who nid = if nid = Env.me () then Me else NotMe
let _address_of id = Env.IntMap.find Env.private_env.locations id

(* let boardcast root value =
  let net = Option.get !Env.private_net in
  match _who root with
  | Me ->
    let count = ref 1 in
    let total = Env.private_env.count in
    let s = Socket.serve_at net (Env.self_address ()) in
    ignore @@ Socket.poll s (fun _ _ _ -> failwith "todo");
    value
  | NotMe ->
    let adr = _address_of root in
    let flow = Socket.connect_to net adr in
    let v = Channel.read flow in
    print_int v;
    print_endline "";
    v
;; *)

type _role =
  | Caller
  | Callee
  | Other

let _decide caller callee =
  match _who caller with
  | Me -> Caller
  | NotMe ->
    (match _who callee with
     | Me -> Callee
     | NotMe -> Other)
;;

type remote_function = string

(** Run a function at a node
    @param id id of the node
    @param f function to execute *)
let run_at id f =
  match _who id with
  | Me -> f ()
  | NotMe -> ()
;;

let plus_1 x = x + 1

let run_funcion (f : remote_function) arg =
  if f = "plus_1"
  then plus_1 arg
  else failwith (Printf.sprintf "function [%s] not found !" f)
;;

type message = Call of remote_function * int

(** f should not be closure *)
let call net caller callee (f : remote_function) (arg : int) : int =
  match _decide caller callee with
  | Caller ->
    let target = _address_of callee in
    Log.trace "caller tring to connect %s" (Socket.string_of_address target);
    let send_wait flow =
      let message = Call (f, arg) in
      Log.trace "caller trying to send f and arg";
      Channel.write flow message;
      Log.trace "caller reading return";
      let res = Channel.read flow in
      Eio.Flow.close flow;
      res
    in
    Socket.connect_to net target send_wait
  | Callee ->
    Log.trace "I am callee";
    let me = Env.self_address () in
    Log.trace "callee setting up server for connection";
    let recv_return counter flow _ =
      Client_counter.increase counter;
      Log.trace "callee reading for f and arg";
      let (Call (f', arg')) = Channel.read flow in
      let res = run_funcion f' arg' in
      Channel.write flow res;
      Eio.Flow.close flow;
      Log.trace "return sends back"
    in
    poll_until net me 1 recv_return;
    -1
  | Other -> -1
;;

(* type value =
  | Local of int
  | Remote of
      { nid : int
      ; vid : int
      }

type store = (int, int) Hashtbl.t

let store = Hashtbl.create 10
let counter = ref 0 *)

(* let define nid value =
  match _who nid with
  | Me ->
    let res = Local value in
    let vid = !counter in
    Hashtbl.add store vid res;
    counter := vid + 1;
    ignore (boardcast nid vid);
    res
  | NotMe ->
    let vid = boardcast nid 2 in
    Remote { nid; vid }
;; *)

(* let print_value v =
   match v with
   | Local x -> print_int x
   | Remote it -> Printf.printf "remote at %d of id %d" it.nid it.vid
   ;; *)
