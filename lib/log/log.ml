let trace fmt =
  Printf.ksprintf
    (fun str ->
      Printf.printf "[actor] - %s\n" str;
      Stdlib.flush stdout)
    fmt
;;
