# Ping-Pang by sockets in OCaml

## OCaml socket docs:
https://ocaml.github.io/ocamlunix/sockets.html

## Run
This will create a cluster with 3 nodes, 1 master with 2 workers.
1. run `run_master.sh` as a process
1. run `run_worker.sh` as a process twice


